#include "data_structure.h"
#include <string>
#include <vector>
#include <cmath>
#include <iostream>

data_structure::data_structure() {
    // Default constructor: Generate an empty data structure
    this->head = nullptr;
    this->tail = nullptr;
    this->size = 0;

}

data_structure::data_structure(std::string input_string) {
    // String constructor: Construct a data structure and store the input string into it
    int i,y;
    this->head = this->tail = 0;
    this->size = 1;
    node *temp;
    std::vector<int>v;
    int result=0;

    //creating the first node for value[0]
    temp = this->head = this->tail=new node(0);

    for(i=0; i<input_string.size(); i++)
    {
        if(input_string[i] == ' ' || input_string[i] == ',') {
            for (y = v.size() - 1; y >= 0; y--) {

                result += v[y] * pow(10, y);
            }

            temp->data = result;
            tail->next = new node(0);
            tail = tail->next;
            tail->prev = temp;
            temp = temp->next;
            result = 0;
            v = {0};

        }

        else
        {
            //assume the list is empty since this is the constructor
          if((int)input_string[i]-'0' == temp->data)
          {
              //If values exist then update the frequency!
              temp->frequency++;
          }
            else {

              v.push_back((int)input_string[i]-'0');
          }
        }

    }


}

data_structure::~data_structure() {
    // Default Destructor: Deconstruct the data structure
    if(this->head == nullptr && this->tail == nullptr )
    {
        delete head, tail;
        return;
    }

    else
    {
        node *temp, *eraser;
        temp = eraser = this->head;
        while((temp!= nullptr))
        {
            temp = temp->next;
            delete eraser;
            eraser = temp;

        }
    }

}

unsigned int data_structure::frequency(int input_character) {
    // Return the number of times the integer is in the data structure.
    node* temp;
    temp = head;

    while(temp != nullptr)
    {
        if(temp->data == input_character)
        {
            return temp->frequency;
        }

        //check each node!
        temp = temp->next;
    }

    return 0;
}

int data_structure::most_frequent() {
    // Return the most frequent number in the data structure. If there is more than one, return the highest value

    node * temp;
    int savedNum=0;
    unsigned savedFre=0;
    temp = head;

    while(temp!= nullptr)
    {
        if(temp->frequency > savedFre || temp->data< savedNum)
        {
            savedFre = temp->frequency;
            savedNum = temp->data;
        }
        temp = temp->next;

    }
    return savedNum;
}

int data_structure::least_frequent() {
    // Return the least frequent number in the data structure. If there is more than one, return the lowest value
    node * temp;
    int savedNum=0;
    unsigned savedFre=0;
    temp = head;

    while(temp!= nullptr)
    {
        if(temp->frequency < savedFre || temp->data > savedNum)
        {
            savedFre = temp->frequency;
            savedNum = temp->data;
        }
        temp = temp->next;

    }
    return savedNum;
}

void data_structure::sort() {
    // Sort the data structure first by frequency, greatest to least and then by value, least to greatest.
    // Example: 1:3,42:4,17:3,11:1,46:1,3:2         sorted: 42:4,1:3,17:3,3:2,11:1,46:1




}

std::istream &operator>>(std::istream &stream, data_structure &structure) {
    // Stream in a string, empty the current structure and create a new structure with the new streamed in string.
    std::string values;
    std::cout<<"Please enter you string of numbers and press enter to terminate"<<std::endl;
    std::getline(stream, values);

    structure.head = nullptr;
    structure.tail = nullptr;
    structure.size = 0;

    int i,y;
    structure.head = structure.tail = 0;
    structure.size = 1;
    node *temp;
    std::vector<int>v;
    int result=0;

    //creating the first node for value[0]
    temp = structure.head = structure.tail=new node(0);

    for(i=0; i<values.size(); i++)
    {
        if(values[i] == ' ' || values[i] == ',')
        {
            for(y=v.size()-1; y>=0; y--)
            {

                result += v[y]*pow(10,y);
            }

            temp->data = result;
            structure.tail->next=new node(0);
            structure.tail = structure.tail->next;
            structure.tail->prev = temp;
            temp = temp->next;
            result = 0;
            v = {0};

        }

        else
        {
            //assume the list is empty since this is the constructor
            if((int)values[i]-'0' == temp->data)
            {
                //If values exist then update the frequency!
                temp->frequency++;
            }
            else {

                v.push_back((int)values[i]-'0');
            }
        }

    }

    return stream;
}

std::ostream &operator<<(std::ostream &stream, const data_structure &structure) {
    // Stream out the data structure
    // Output in this format "<integer>:<frequency>,<integer>:<frequency>,<integer>:<frequency>"
    node* temp = structure.head;


    while(temp != nullptr)
    {
        std::cout<<temp->data<<temp->frequency<<", ";
    }
    std::cout<<std::endl;
    return stream;

}
