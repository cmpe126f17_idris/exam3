//
// Created by amer0 on 12/7/2017.
//

#ifndef CMPE126F17_EXAM3A_NODE_H
#define CMPE126F17_EXAM3A_NODE_H

class node {
public:
    node *next, *prev;
    int data;
    unsigned frequency;
    explicit node(unsigned int data) : data(data), next(nullptr), prev(nullptr), frequency(1) {}

};


#endif //CMPE126F17_EXAM3A_NODE_H
